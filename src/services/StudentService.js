import axios from "axios";

const REST_API_BASE_URL = 'http://192.168.59.100:32001/api/v1/students';
const REPLACEMENT_REST_API_BASE_URL = 'http://192.168.100.240:32001/api/v1/students';

const logRequestDetails = (method, url, response) => {
    console.log(`HTTP Method: ${method}`);
    console.log(`Request Path: ${url}`);
    if (response) {
        console.log(`Response Code: ${response.status}`);
    }
};

export const listStudents = async () => {
    const url = REST_API_BASE_URL;
    const method = 'GET';
    try {
        const response = await axios.get(url);
        logRequestDetails(method, url, response);
        return response.data;
    } catch (error) {
        logRequestDetails(method, url, error.response);
        console.error('Error fetching students:', error);
        try {
            // If unsuccessful, try connecting to the replacement URL
            const replacementResponse = await axios.get(REPLACEMENT_REST_API_BASE_URL);
            console.log('Successfully fetched students from replacement URL');
            logRequestDetails(method, REPLACEMENT_REST_API_BASE_URL, replacementResponse);
            return replacementResponse.data;
        } catch (replacementError) {
            logRequestDetails(method, REPLACEMENT_REST_API_BASE_URL, replacementError.response);
            console.error('Error fetching students from replacement URL:', replacementError);
            return [];
        }
    }
};

export const addStudent = async (student) => {
    const url = REST_API_BASE_URL;
    const method = 'POST';
    try {
        const response = await axios.post(url, student);
        logRequestDetails(method, url, response);
        return response.data;
    } catch (error) {
        logRequestDetails(method, url, error.response);
        console.error('Error creating student:', error);
        return [];
    }
};

export const updateStudent = async (studentId, student) => {
    const url = `${REST_API_BASE_URL}/${studentId}`;
    const method = 'PUT';
    try {
        const response = await axios.put(url, student);
        logRequestDetails(method, url, response);
        return response.data;
    } catch (error) {
        logRequestDetails(method, url, error.response);
        console.error('Error updating student:', error);
        return [];
    }
};

export const getStudent = async (studentId) => {
    const url = `${REST_API_BASE_URL}/${studentId}`;
    const method = 'GET';
    try {
        const response = await axios.get(url);
        logRequestDetails(method, url, response);
        return response.data;
    } catch (error) {
        logRequestDetails(method, url, error.response);
        console.error('Error getting student:', error);
        return [];
    }
};

export const removeStudent = async (studentId) => {
    const url = `${REST_API_BASE_URL}/${studentId}`;
    const method = 'DELETE';
    try {
        const response = await axios.delete(url);
        logRequestDetails(method, url, response);
        return response.data;
    } catch (error) {
        logRequestDetails(method, url, error.response);
        console.error('Error deleting student:', error);
        return [];
    }
};
